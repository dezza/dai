err_report() {
  local parent_lineno="$1"
  local parent_err="$2"
  echo "ERROR: line $parent_lineno: exit ($parent_err)"
  trap - ERR
  exit 1
}

trap 'err_report $LINENO $?' ERR

echo "Generate locales"
locale-gen

echo "Setting keymap: $keymap"
echo 'KEYMAP=${keymap}' > /etc/vconsole.conf

echo "Linking timezone $timezone to /etc/localtime"
ln -s /usr/share/zoneinfo/${timezone} /etc/localtime

echo "Saving to hardware clock"
hwclock --systohc --utc

# TODO conditional on GPT/UEFI/EFI systems
echo "Installing bootloader"
bootctl install

echo "Creating ArchLinux boot entry"
cat << BOOTENTRY > /boot/loader/entries/arch.conf
title          Arch Linux
linux          /vmlinuz-linux
initrd         /initramfs-linux.img
options        root=$root_device rw
BOOTENTRY

cat << BOOTENTRY > /boot/loader/entries/loader.conf
timeout 3
default arch
BOOTENTRY

echo "Setting hostname"
echo '$hostname' > /etc/hostname

echo "Change root password to temporary password"
echo root:"$(openssl passwd -1 $root_password)" | chpasswd -e

# Enable wired device on next bootup or not?
if [ $enable_network ]; then
  echo "Enabling network"
  systemctl enable dhcpcd@${iface}.service
fi

echo "Unmount temporary install mountpoint"

umount -R /mnt
clear
echo "Rebooting ..."
reboot

trap - ERR
