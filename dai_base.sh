# NOTE: Enable EFI in VirtualBox for this script to work.
#
printf "Dandy ArchLinux Inflator\n - https://bitbucket.org/dezza/dai\n\n"
#

: <<CONFIGURATION
Adjust variables below accordingly to your setup
CONFIGURATION

repo_url="git@bitbucket.org:dezza/dai.git"

# additional packages
packages=()

export keymap="dk"
export timezone="Europe/Copenhagen"

disk_device="/dev/sda"  # main disk
efi_device="/dev/sda1"  # efi/fat32 UEFI/GPT partition
export root_device="/dev/sda2" # root partition /
auto_disk=true # 0 = cfdisk, 1 = script (parted)

hostname="adrafinil"
export root_password="changeme"

enable_network=true
network_type="wired" # or wifi, or interface

enable_ssh=true

err_report() {
  local parent_lineno="$1"
  local parent_err="$2"
  echo "ERROR: line $parent_lineno: exit ($parent_err)"
  trap - ERR
  kill -SIGINT $$
}

trap 'err_report $LINENO $?' ERR
arch_chroot() {
  arch-chroot /mnt /bin/bash -c "${1}"
}



if [ "$enable_ssh" ]; then
  # TODO: ssh enable, cumbersome to keep retyping (no paste) in virtualbox and rebooting all da freakin time!
  echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
  echo root:"$(openssl passwd -1 $root_password)" | chpasswd -e
  systemctl start sshd
  #
fi

if [ "$enable_network" ]; then
  # TODO/FIXME: Maybe move net enable after autologin code? after testing! make a var for SSH!
  echo "Enabling dhcpcd systemd service"
  if [ "$network_type" = "wired" ]; then
    iface_arr=(/sys/class/net/en*(N))
  elif [ "$network_type" = "wifi" ]; then
    iface_arr=(/sys/class/net/wlp*(N))
  else
    iface="$network_type"
  fi

  [[ -z iface_arr ]] && iface=$(basename "${iface_arr[1]}")
  # TODO: Wifi selection?
  systemctl enable dhcpcd@"${iface}".service
fi



printf "\nStarting ...\n"
loadkeys $keymap
timedatectl set-ntp true
timedatectl status

if [ "$auto_disk" = true ]; then
  echo "Imma partitioning n' shit fo' y'all !"
  parted -s $disk_device mklabel gpt #warning; overwrites partition table - condition if
  parted -s $disk_device mkpart ESP fat32 1MiB 513MiB
  parted -s $disk_device set 1 boot on 
  # no swap
  parted -s $disk_device mkpart primary ext4 513MiB 100%
  mkfs.ext4 -F $root_device
  mkfs.fat -F32 $efi_device
  # TODO: SWAP, LVM, BOOT-PARTITION, CRYPT? and more?
else
  cfdisk $disk_device
fi


echo "Mounting root device $root_device"
mount $root_device /mnt
echo "Creating /boot directory"
mkdir -p /mnt/boot
echo "Mounting /boot directory with $root_device"
mount $efi_device /mnt/boot
printf "\n\n"


echo "Installing base system (pacstrap)"
# Temporarily comment out to make virtualbox debugging easier and faster with snapshots
pacstrap /mnt base base-devel \
  git \
  openssh \
  $packages
# TODO: Put custom packages above in a variable/array ^

echo "Generating fstab"
# TODO: genfstab -L "use labels" replace it! but make labels first!
genfstab -U /mnt > /mnt/etc/fstab

sed -i '/^#en_US.UTF-8 UTF-8/s/^#//' /etc/locale.gen



arch_chroot "git clone $repo_url /root/dai"
arch_chroot "./root/dai/dai_chroot.sh"
trap - ERR
