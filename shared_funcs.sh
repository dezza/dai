#!/usr/bin/env bash

: << C
Shared functions library?
- will prevent a single curl command?

On the other hand - after a reboot, you will run a different command anyways
to continue the install?

Unless we can automate it via ssh or something via the initial setup! LOL!
C

failcheck() {
    "$@"
    local exitstatus=$?
    if [ $exitstatus -ne 0 ]; then
        echo "Error with $1" >&2
        exit 1
    fi
    return $exitstatus
}

#failcheck "cd lol"
#failcheck "cd john"
#echo MEANTTOFAIL
